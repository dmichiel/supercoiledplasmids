Codes to generate supercoiled plasmids

1. Run..lam is the LAMMPS command file
2. Restart..data is a restart file for N=40, M=400 and supercoiling s=0.02
3. GenLammpsInput.c++ is a c++ to generate initial conformations for different 
number of plasmids and lengths. these are initialised as perfect circles that are 
tilted in space. The beads-patch frame is arranged as a flat ribbon (initial 
linking number 0) so that imposing a relaxed pitch induces supercoiling.